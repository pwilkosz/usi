<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('doctor/{id?}', array('as' => 'doctor.get', 'uses' => 'DoctorController@show'));
Route::get('doctor/{id}/appointment/{app_id?}', array('as' => 'doctor.get', 'uses' => 'DoctorController@appointments'));
Route::get('doctor/speciality/{spec_id}', array('as' => 'doctor.get', 'uses' => 'DoctorController@specialities'));
Route::post('doctor/create', array('as' => 'doctor.post', 'uses' => 'DoctorController@create'));
Route::post('doctor/{id}/edit', array('as' => 'doctor.post', 'uses' => 'DoctorController@edit'));
Route::delete('doctor/{id}/delete', array('as' => 'doctor.post', 'uses' => 'DoctorController@delete'));

/*---------------SPECIALITIES------------------------------*/

Route::get('speciality/{id?}', array('as' => 'speciality.get', 'uses' => 'SpecialityController@show'));
Route::post('speciality/{id}/edit', array('as' => 'doctor.post', 'uses' => 'SpecialityController@edit'));


/*---------------APPOINTMENTS------------------------------*/

Route::get('appointment/{id?}', array('as' => 'appointment.get', 'uses' => 'AppointmentController@show'));
Route::post('appointment/create', array('as' => 'appointment.post', 'uses' => 'AppointmentController@create'));
Route::delete('appointment/{id}/delete', array('as' => 'appointment.post', 'uses' => 'AppointmentController@delete'));
Route::post('appointment/{id}/edit', array('as' => 'appointment.post', 'uses' => 'AppointmentController@edit'));

/*---------------PATIENT------------------------------*/
Route::get('patient/{patient_id}', array('as' => 'read_patient', 'uses' => 'PatientController@get'));
Route::get('patient', array('as' => 'read_patients', 'uses' => 'PatientController@show'));
Route::get('patient/{patient_id}/appointment', array('as' => 'read_patient_appointments', 'uses' => 'PatientController@appointments'));
Route::get('patient/{patient_id}/appointment/{appointment_id}', array('as' => 'read_patient_appointment', 'uses' => 'PatientController@appointment'));
Route::delete('patient/{patient_id}/delete', array('as' => 'delete_patient', 'uses' => 'PatientController@delete'));
Route::post('patient/create', array('as' => 'create_patient', 'uses' => 'PatientController@create'));
Route::post('patient/{patient_id}/edit', array('as' => 'edit_patient', 'uses' => 'PatientController@edit'));
Route::post('patient/{patient_id}/appointment', array('as' => 'read_patient_appointments_by_date', 'uses' => 'PatientController@date'));

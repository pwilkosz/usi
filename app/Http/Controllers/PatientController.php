<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Doctor;
//use App\Speciality;
use App\Appointment;
use App\Patient;

class PatientController extends Controller {
    //put your code here
   public function get(){
       $patient_id = request() ->route("patient_id");
       $patient = Patient::find($patient_id);
            return response()->json($patient);
}

    public function show(){
        $patients = Patient::all();
        return response()->json($patients);
}

    public function appointments(){
   
        $id = request() ->route("patient_id");
        $patient = Patient::find($id);
        $appointments = $patient->appointments;
        return response()->json($appointments);
    }
    
        public function appointment(){
        $id = request() ->route("patient_id");
        $id_app = request() ->route("appointment_id");
        
        $patientAppointments = Appointment::where('PATIENT_id', $id)->where('id', $id_app)->get();
	return $patientAppointments->toJson();
        //$patient = Patient::find($id);
        //$app = Appointment::find($id_app);
        //$appointment = $patient->Appointment::find($id_app);
        //return response()->json($appointment);
    }

    public function delete(){
        $id = request() ->route("patient_id");
        $patient = Patient::find($id);
	$patientId = $patient->id;
	$patient->delete();
	return json_encode(["id" => $patientId]);
        
}

    public function create(Request $data)
    {
    $patient = new Patient();

    $patient->first_name = $data->input('first_name');
    $patient->last_name = $data->input('last_name');
    $patient->phone = $data->input('phone');
    $patient->gender = $data->input('gender');
    $patient->birthday = $data->input('birthday');
    $patient->email = $data->input('email');
    $patient->save();
    return json_encode(["id" => $patient->id]);
    }
    
    public function edit(Request $data)
            {
        
        $id = request() ->route("patient_id");
        $patient = Patient::find($id);

		if($data->has('first_name')){
			$patient->first_name = $data->input('first_name');
		}
		if($data->has('last_name')){
			$patient->last_name = $data->input('last_name');
		}
		if($data->has('phone')){
			$patient->phone = $data->input('phone');
		}
		if($data->has('gender')){
			$patient->gender = $data->input('gender');
		}
		if($data->has('birthday')){
			$patient->birthday = $data->input('birthday');
		}
		if($data->has('email')){
			$patient->email = $data->input('email');
		}
		
		$patient->save();
		
		return json_encode($patient);
    }
    
    
    
        public function date(Request $data)
            {
        
        $id = request() ->route("patient_id");
        $patient = Patient::find($id);
         
        
        $app = Appointment::find($data->input('date'));

        
		$app->Patient()->associate($patient);
  
                
                if($data->has('DOCTOR_id')){
			$app->DOCTOR_id = $data->input('DOCTOR_id');
		}
                if($data->has('PATIENT_id')){
		$app->PATIENT_id = $data->input('PATIENT_id');
                }
                if($data->has('date')){
		$app->date = $data->input('date');
                }
                if($data->has('duration')){
		$app->duration = $data->input('duration');
                }
		$app->save();
		
		$app->save();
		
		return json_encode($app);
    }
    
    
}
?>

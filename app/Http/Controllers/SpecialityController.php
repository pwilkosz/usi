<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Doctor;
use App\Speciality;
use App\Appointment;

class SpecialityController extends Controller
{
    //
    public function show($id = null){
		if ($id == null){
			$specs = Speciality::all();
		}
        else {
			$specs = Speciality::find($id);
		}
		return $specs->toJson();
    }
	
	public function edit(Request $request, $id){
		$spec = Speciality::find($id);
		if($request->has('price_per_appointment')){
			$spec->price_per_appointment = $request->input('price_per_appointment');
		}
		if($request->has('name')){
			$spec->name = $request->input('name');
		}
		$spec->save();
		
		return json_encode($spec);
		
		
	
	}
	
	
}


?>
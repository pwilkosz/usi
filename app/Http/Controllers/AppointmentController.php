<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Doctor;
use App\Appointment;
use App\Patient;
use App\Speciality;

class AppointmentController extends Controller 
{
    public function show($id = null){
        if ($id == null){
			$appointment = Appointment::all();
		}
        else {
			$appointment = Appointment::find($id);
		}
		return $appointment->toJson();
    }
    
    public function create(Request $request){

                $doct = Doctor::find($request->input('doctor_id'));
                $patt = Patient::find($request->input('patient_id'));       
                
                $appo = new Appointment;                
                $appo->Doctor()->associate($doct);
                $appo->Patient()->associate($patt);
                $appo->date = $request->input('date');
                $appo->duration = $request->input('duration');
                $appo->save();
                
                return json_encode(["id" => $appo->id]);
                
	}
    
        public function delete($id){
		$appo = Appointment::find($id);
		$appoId = $appo->id;
		$appo->delete();
		return json_encode(["id" => $appoId]);
		
	}
        
       public function edit($id, Request $request){         
                $appo = Appointment::find($id);
		if($request->has('doctor_id')){
                        $doct = Doctor::find($request->input('doctor_id'));
			$appo-> Doctor()->associate($doct);			
		}
                
                if($request->has('speciality_id')){
                        $spec = Speciality::find($request->input('speciality_id'));
			$appo-> Speciality()->associate($spec);			
		}
                
		if($request->has('date')){
			$appo->date = $request->input('date');
		}
                
		if($request->has('duration')){
			$appo->duration = $request->input('duration');
		}		
		
		$appo->save();
		
		return json_encode($appo);
	}
        
        
        
        
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Doctor;
use App\Speciality;
use App\Appointment;

class DoctorController extends Controller
{
    //
    public function show($id = null){
		if ($id == null){
			$doctors = Doctor::all();
		}
        else {
			$doctors = Doctor::find($id);
		}
		return $doctors->toJson();
    }
	
	public function create(Request $request){
		$spec = Speciality::find($request->input('speciality_id'));
		
		$doc = new Doctor;
		$doc->Speciality()->associate($spec);
		$doc->first_name = $request->input('first_name');
		$doc->last_name = $request->input('last_name');
		$doc->phone = $request->input('phone');
		$doc->gender = $request->input('gender');
		$doc->birthday = $request->input('birthday');
		$doc->email = $request->input('email');
		$doc->room = $request->input('room');
		$doc->save();
		
		return json_encode(["id" => $doc->id]);
	}
	
	public function appointments($id, $app_id = null){
		$doctorAppointments = Appointment::where('DOCTOR_id', $id)->get();
		if($app_id != null){
			$doctorAppointments = Appointment::where('DOCTOR_id', $id)->where('id', $app_id)->get();
			return $doctorAppointments->toJson();
		}
		else return $doctorAppointments->toJson();
	}
	
	public function specialities($spec_id){
		$doctorsBySpeciality = Doctor::where('SPECIALITY_id', $spec_id)->get();
		return $doctorsBySpeciality->toJson();
	}
	
	public function edit($id, Request $request){
		$doc = Doctor::find($id);
		if($request->has('speciality_id')){
			$spec = Speciality::find($request->input('speciality_id'));
			$doc->Speciality()->associate($spec);
		}
		if($request->has('first_name')){
			$doc->first_name = $request->input('first_name');
		}
		if($request->has('last_name')){
			$doc->last_name = $request->input('last_name');
		}
		if($request->has('phone')){
			$doc->phone = $request->input('phone');
		}
		if($request->has('gender')){
			$doc->gender = $request->input('gender');
		}
		if($request->has('birthday')){
			$doc->birthday = $request->input('birthday');
		}
		if($request->has('email')){
			$doc->email = $request->input('email');
		}
		if($request->has('room')){
			$doc->room = $request->input('room');
		}
		
		$doc->save();
		
		return json_encode($doc);
	}
	
	public function delete($id){
		$doc = Doctor::find($id);
		$docId = $doc->id;
		$doc->delete();
		return json_encode(["id" => $docId]);
		
	}
}


?>